import os
import json
import yaml
import falcon

# Manager class for repository metadata tree
class RepositoryMetadataManager(object):

	# Initial Setup
	def __init__(self, metadataPath):
		# Setup our data stores
		self.knownProjects    = []
		self.knownPaths       = {}
		self.knownIdentifiers = {}

		# Now load the information we've been given
		for currentPath, subdirectories, filesInFolder in os.walk( metadataPath, topdown=False, followlinks=False ):
			# Do we have a metadata.yaml file?
			# If we don't then there is nothing for us to do here
			if 'metadata.yaml' not in filesInFolder:
				# We're not interested then....
				continue

			# Load the metadata for this project
			metadataPath = os.path.join( currentPath, 'metadata.yaml' )
			metadataFile = open( metadataPath, 'rt', encoding='utf8')
			metadata = yaml.safe_load(metadataFile)

			# But if it is active then we have a winner!
			# Grab some bits of information we will need later...
			path = metadata['repopath']
			identifier = metadata['identifier']

			# Add the project to our list of projects as well as the appropriate maps
			self.knownProjects.append( metadata )
			self.knownPaths[ path ] = metadata
			self.knownIdentifiers[ identifier ] = metadata
